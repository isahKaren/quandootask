//
//  UserEndpointTests.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 14.03.24.
//

import XCTest
@testable import QuandooTask

final class UserEndpointTests: XCTestCase {
    
    var client: UsersEndpoint!
    var mockNetworking: MockNetworking!

    override func setUp() {
        super.setUp()
        mockNetworking = MockNetworking()
        client = UsersEndpoint(networking: mockNetworking)
        mockNetworking.mockData = [MockData.user]
    }

    override func tearDown() {
        client = nil
        mockNetworking = nil
        super.tearDown()
    }

    func testFetchUsersCorrectURL() {
        let expectedURL = URL(string: "https://jsonplaceholder.typicode.com/users")!
        
        _ = client.fetch().sink(receiveCompletion: { _ in }) { _ in }
        
        XCTAssertEqual(mockNetworking.capturedURL, expectedURL)
    }
}
