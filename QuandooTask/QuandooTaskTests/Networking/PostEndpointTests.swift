//
//  PostEndpointTests.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 14.03.24.
//

import XCTest
@testable import QuandooTask

final class PostEndpointTests: XCTestCase {
    
    var client: PostsEndpoint!
    var mockNetworking: MockNetworking!

    override func setUp() {
        super.setUp()
        mockNetworking = MockNetworking()
        client = PostsEndpoint(networking: mockNetworking)
        mockNetworking.mockData = [MockData.posts]
    }

    override func tearDown() {
        client = nil
        mockNetworking = nil
        super.tearDown()
    }

    func testFetchUsersCorrectURL() {
        let expectedURL = URL(string: "https://jsonplaceholder.typicode.com/posts?userId=0")!
        
        _ = client.fetch(userId: 0).sink(receiveCompletion: { _ in }) { _ in }
        
        XCTAssertEqual(mockNetworking.capturedURL, expectedURL)
    }
}

