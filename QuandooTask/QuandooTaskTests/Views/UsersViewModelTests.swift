//
//  UsersViewModelTests.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 03.03.24.
//

import XCTest
import Combine
@testable import QuandooTask

final class UsersViewModelTests: XCTestCase {

    var viewModel: UsersViewModel!
    var mockClient: MockUsersClient!

    override func setUp() {
        super.setUp()
        let mockNavigationController = UINavigationController()
        let coordinator = Coordinator(navigationController: mockNavigationController)
        mockClient = MockUsersClient(result: .success([]))
        viewModel = UsersViewModel(service: mockClient, coordinator: coordinator)
    }

    override func tearDown() {
        viewModel = nil
        mockClient = nil
        super.tearDown()
    }

    func testFetchUpdatesUsers() {
        let expectation = expectation(description: "Expectation")
        let mockUsers = [MockData.user]
        let mockClient = MockUsersClient(result: .success(mockUsers))
        let mockNavigationController = UINavigationController()
        let coordinator = Coordinator(navigationController: mockNavigationController)
        viewModel = UsersViewModel(service: mockClient, coordinator: coordinator)

        let dataPublisherExpectation = viewModel.$users
            .dropFirst()
            .sink { users in
                XCTAssertEqual(users, mockUsers)
                expectation.fulfill()
            }

        viewModel.fetchData()
        
        waitForExpectations(timeout: 1.0) { error in
            XCTAssertNil(error, "Error: \(error.debugDescription)")
        }

        dataPublisherExpectation.cancel()
    }
}
