//
//  MockData.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 13.03.24.
//

import Foundation
@testable import QuandooTask

enum MockData {
    static let user = User(id: 9999,
                           name: "Isabela",
                           username: "isah",
                           email: "isahkaren@123",
                           address: Address(street: "Corinthstr",
                                            suite: "01",
                                            city: "Berlin",
                                            zipcode: "10245"),
                           phone: "+4917011111")
    
    static let posts = Post(userId: 00, id: 11, title: "Test1", body: "Tests")
}
