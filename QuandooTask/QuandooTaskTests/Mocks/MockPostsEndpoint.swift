//
//  MockPostsEndpoint.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 13.03.24.
//

import Foundation
import Combine
@testable import QuandooTask

final class MockPostClient: PostsEndpointProtocol {

    let result: Result<[Post], Error>

    init(result: Result<[Post], Error>) {
        self.result = result
    }

    func fetch(userId: Int) -> AnyPublisher<[Post], Error> {
        return result.publisher.eraseToAnyPublisher()
    }
}
