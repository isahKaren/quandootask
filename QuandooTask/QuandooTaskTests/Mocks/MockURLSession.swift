//
//  MockURLSession.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 13.03.24.
//

import Foundation
import Combine
@testable import QuandooTask

final class MockURLSession: URLSessionProtocol {
    var dataTaskPublisherCallCount = 0
    var result: Result<(data: Data, response: URLResponse), URLError> = .failure(URLError(.badServerResponse))
    
    func dataTaskPublisher(for url: URL) -> URLSession.DataTaskPublisher {
        dataTaskPublisherCallCount += 1
        fatalError()
    }
}
