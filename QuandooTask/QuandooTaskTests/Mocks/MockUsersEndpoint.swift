//
//  MockUsersEndpoint.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 13.03.24.
//

import Foundation
import Combine
@testable import QuandooTask

final class MockUsersClient: UsersEndpointProtocol {
  
    let result: Result<[User], Error>

    init(result: Result<[User], Error>) {
        self.result = result
    }

    func fetch() -> AnyPublisher<[User], Error> {
        return result.publisher.eraseToAnyPublisher()
    }
}

