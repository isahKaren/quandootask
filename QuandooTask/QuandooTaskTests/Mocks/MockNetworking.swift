//
//  MockNetworking.swift
//  QuandooTaskTests
//
//  Created by Isabela Karen on 13.03.24.
//

import Foundation
import Combine
@testable import QuandooTask

final class MockNetworking: Networking {
    var capturedURL: URL?
    var mockData: [Decodable]?

    func request<T: Decodable>(url: URL) -> AnyPublisher<T, Error> {
        capturedURL = url
        return Just(mockData as! T)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func downloadData(url: URL) -> AnyPublisher<Data, URLError> {
        capturedURL = url
        return Just(Data())
            .setFailureType(to: URLError.self)
            .eraseToAnyPublisher()
    }
}

