//
//  URLSessionExtension.swift
//  QuandooTask
//
//  Created by Isabela Karen on 05.03.24.
//

import Foundation

protocol URLSessionProtocol {
    func dataTaskPublisher(for url: URL) -> URLSession.DataTaskPublisher
}

extension URLSession: URLSessionProtocol {}
