//
//  PostModel.swift
//  QuandooTask
//
//  Created by Isabela Karen on 02.03.24.
//

import Foundation

struct Post: Decodable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}
