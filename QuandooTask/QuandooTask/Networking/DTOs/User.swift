//
//  UserModel.swift
//  QuandooTask
//
//  Created by Isabela Karen on 02.03.24.
//

import Foundation

struct User: Decodable, Equatable {
    let id: Int
    let name: String
    let username: String
    let email: String
    let address: Address
    let phone: String
}

struct Address: Decodable, Equatable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String

    var fullAddress: String {
        return "\(street), \(suite), \(city), \(zipcode)"
    }
}
