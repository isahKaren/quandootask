//
//  PostsEndpoint.swift
//  QuandooTask
//
//  Created by Isabela Karen on 05.03.24.
//

import Foundation
import Combine

protocol PostsEndpointProtocol {
    func fetch(userId: Int) -> AnyPublisher<[Post], Error>
}

final class PostsEndpoint: Endpoint, PostsEndpointProtocol {
    
    func fetch(userId: Int) -> AnyPublisher<[Post], Error> {
        return fetch(path: "posts?userId=\(userId)")
    }
}

