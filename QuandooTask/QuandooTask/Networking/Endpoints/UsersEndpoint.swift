//
//  UsersEndpoint.swift
//  QuandooTask
//
//  Created by Isabela Karen on 05.03.24.
//

import Foundation
import Combine

protocol UsersEndpointProtocol {
    func fetch() -> AnyPublisher<[User], Error>
}

final class UsersEndpoint: Endpoint, UsersEndpointProtocol {

    func fetch() -> AnyPublisher<[User], Error> {
        return fetch(path: "users")
    }
}
