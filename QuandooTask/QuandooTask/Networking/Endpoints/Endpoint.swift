//
//  Endpoint.swift
//  QuandooTask
//
//  Created by Isabela Karen on 05.03.24.
//

import Foundation
import Combine

class Endpoint {
    private let networking: Networking
    private let baseURLString = "https://jsonplaceholder.typicode.com/"
    
    init(networking: Networking = Client()) {
        self.networking = networking
    }

    func fetch<T: Decodable>(path: String) -> AnyPublisher<T, Error> {
        let urlString = baseURLString + path
        
        guard let url = URL(string: urlString) else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }

        return networking.request(url: url).eraseToAnyPublisher()
    }
}
