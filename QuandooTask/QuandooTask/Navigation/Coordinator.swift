//
//  Coordinator.swift
//  QuandooTask
//
//  Created by Isabela Karen on 08.03.24.
//

import SwiftUI
import UIKit

final class Coordinator {
    var navigationController: UINavigationController?

    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func navigateToPostView(with viewModel: PostsViewModel) {
        let postView = PostsView(viewModel: viewModel)
        let hostingController = UIHostingController(rootView: postView)

        navigationController?.pushViewController(hostingController, animated: true)
    }
}
