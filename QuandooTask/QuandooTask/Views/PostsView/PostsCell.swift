//
//  PostsCell.swift
//  QuandooTask
//
//  Created by Isabela Karen on 08.03.24.
//

import SwiftUI

struct PostsCell: View {
    var userName: String
    var post: Post
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            HStack(spacing: 10) {
                Text(String(userName.prefix(1)))
                    .foregroundColor(.white)
                    .frame(width: 40, height: 40)
                    .background(Color.yellow)

                Text(userName)
                    .font(.headline)
            }

            Text(post.title)
                .font(.subheadline)
                .foregroundColor(.secondary)
            
            Text(post.body)
                .font(.body)
        }
        .padding()
    }
}

#Preview {
    PostsCell(userName: "Isabela", post: Post(userId: 5, id: 2, title: "Post Title", body: "This is a test post"))
}
