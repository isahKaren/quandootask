//
//  PostsViewModel.swift
//  QuandooTask
//
//  Created by Isabela Karen on 08.03.24.
//

import Foundation
import Combine

final class PostsViewModel: ObservableObject {
    
    @Published var posts: [Post] = []
    @Published var user: User
    
    private var cancellables = [AnyCancellable]()
    private let service: PostsEndpointProtocol
    
    init(service: PostsEndpointProtocol = PostsEndpoint(), user: User) {
        self.service = service
        self.user = user
    }

    func fetch() {
        service.fetch(userId: user.id)
            .receive(on: DispatchQueue.main)
            .sink { _ in } receiveValue: { [weak self] items in
                self?.posts = items
            }
            .store(in: &cancellables)
    }
}
