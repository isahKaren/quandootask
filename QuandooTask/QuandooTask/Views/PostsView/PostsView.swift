//
//  PostsView.swift
//  QuandooTask
//
//  Created by Isabela Karen on 08.03.24.
//

import SwiftUI
import Combine

struct PostsView: View {

    @ObservedObject var viewModel: PostsViewModel
    
    var body: some View {
        List(viewModel.posts.indices, id: \.self) { index in
            PostsCell(userName: viewModel.user.name, post: viewModel.posts[index])
        }
        .onAppear {
            viewModel.fetch()
        }
    }
}

#Preview {
    PostsView(viewModel: .init(user: User(
        id: 1, name: "Isabela", username: "isah",
        email: "isahkaren.com",
        address: Address(
            street: "straße", suite: "38", city: "Berlin", zipcode: "10245"
        ),
        phone: "1234678"
    )))
}
