//
//  UsersViewModel.swift
//  QuandooTask
//
//  Created by Isabela Karen on 03.03.24.
//

import Foundation
import Combine

final class UsersViewModel {

    private let service: UsersEndpointProtocol
    private var cancellables = [AnyCancellable]()
    private let coordinator: Coordinator

    @Published var users: [User] = []

    init(service: UsersEndpointProtocol = UsersEndpoint(), coordinator: Coordinator) {
        self.service = service
        self.coordinator = coordinator
    }

    func didSelect(user: User) {
        coordinator.navigateToPostView(with: .init(user: user))
    }

    func fetchData() {
        service.fetch()
            .receive(on: DispatchQueue.main)
            .sink { _ in } receiveValue: { [weak self] items in
                self?.users = items
            }
            .store(in: &cancellables)
    }
}


