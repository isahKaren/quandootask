//
//  UsersView.swift
//  QuandooTask
//
//  Created by Isabela Karen on 03.03.24.
//

import UIKit
import Combine

final class UsersView: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel: UsersViewModel
    private var users: [User] = []
    private var cancellables = [AnyCancellable]()
    private let cellIdentifier = "usersCellIdentifier"

    init(viewModel: UsersViewModel) {
        self.viewModel = viewModel
        let nibName = String(describing: UsersView.self)
        super.init(nibName: nibName, bundle: Bundle(for: UsersView.self))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "UsersCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        bind()
        viewModel.fetchData()
    }

    private func bind() {
      viewModel.$users
        .receive(on: DispatchQueue.main)
        .sink { [weak self] array in
            self?.users = array
            self?.tableView.reloadData()
        }
        .store(in: &cancellables)
    }
}
extension UsersView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! UsersCell

        let user = users[indexPath.row]
        cell.setupCell(with: user)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(user: users[indexPath.row])
    }
}
