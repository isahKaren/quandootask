//
//  UsersCell.swift
//  QuandooTask
//
//  Created by Isabela Karen on 06.03.24.
//

import UIKit

final class UsersCell: UITableViewCell {
    
    @IBOutlet weak var userInitialLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var screenNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    

    func setupCell(with user: User) {
        if let firstLetter = user.name.prefix(1).first {
            userInitialLabel.text = String(firstLetter)
        }
        userNameLabel.text = user.name
        screenNameLabel.text = "@\(user.username)"
        emailLabel.text = user.email
        addressLabel.text = user.address.fullAddress
    }
}
