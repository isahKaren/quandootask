# QuandooTask

## Description

This is a simple application with 2 screens, the first screen displays a list of users with the data:

- name
- email
- username
- address

the second screen has the posts from the selected user :

- user name
- post title
- post body


## Task

Application should contain 2 screens – User List and Post List. Special attention should be paid to code organization. Please keep it well organised and testable.

The first screen should display a list of all users.
Should be done using UIKit

Post List screen, should be displayed when one of users from the first screen is selected. This screen should display all posts from selected user.
Should be done using SwiftUI

